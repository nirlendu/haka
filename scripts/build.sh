#!/bin/bash

if [ $1 == "testing" -o $1 == "development" -o $1 == "staging" -o $1 == "pre-production" -o $1 == "production" ];
then
	if [ $1 == "testing" -o $1 == "development" -o $1 == "staging" ]; then
		cd ../Prithvi/prithvi-app/
		make build
	fi
	export NODE_ENV=$1
	if [ $1 == "production" ]; then
		cd ../Prithvi/prithvi-app/
		make build
		scp -i ../../panally.pem app/target/panally-app-1.0.0.jar ubuntu@ec2-52-66-179-40.ap-south-1.compute.amazonaws.com:~/production/
	fi
else
	echo "Please enter one of the following - testing, development, staging, pre-production, production"
fi