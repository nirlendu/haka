#!/bin/bash

if [ $1 == "testing" -o $1 == "development" -o $1 == "staging" -o $1 == "pre-production" -o $1 == "production" ];
then
	if [ $1 == "testing" -o $1 == "development" -o $1 == "staging" ]; then
		cd ../Prithvi/prithvi-app/
		make run-dev
	fi
	export NODE_ENV=$1
	if [ $1 == "production" ]; then
		sudo kill $(sudo lsof -t -i:9000)
		nohup java -jar ../production/panally-app-1.0.0.jar server config/prod/config.yml &
	fi
else
	echo "Please enter one of the following - testing, development, staging, pre-production, production"
fi