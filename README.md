# Haka 

Deploy, execute & config scripts for Prithvi

## To use

First, you need to clone Prithvi repo

```sh
cd ..
git clone https://gitlab.com/panally/prithvi.git
```
Then,

To only build for development (add appropriate argument for other environments),

```sh
bash scripts/build.sh development
```

Likewise, to deploy,

```sh
bash scripts/deploy.sh development
```

And visit <http://localhost:9000/>.